from tensorflow.keras.applications import VGG16, imagenet_utils
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from sklearn.preprocessing import LabelEncoder
import dataset_writer
from imutils import paths
import numpy as np
import progressbar
import random
import os
from constants import (
    BATCH_SIZE,
    BUFFER_SIZE,
    DATASET_PATH,
    OUTPUT_PATH,
    TARGET_SIZE
)


def main():
    print("[INFO] loading dataset...")
    image_paths = list(paths.list_images(DATASET_PATH))
    random.shuffle(image_paths)

    labels = [p.split(os.path.sep)[-2] for p in image_paths]
    le = LabelEncoder()
    labels = le.fit_transform(labels)

    print("[INFO] loading network...")
    model = VGG16(weights='imagenet', include_top=False)

    dataset = dataset_writer.HDF5DatasetWriter((len(image_paths), 512, 7, 7), OUTPUT_PATH, data_key='features', buf_size=BUFFER_SIZE)
    # dataset.store_class_labels(le.classes_)

    widgets = ["Extracting features: ", progressbar.Percentage(), " ", progressbar.Bar(), " ", progressbar.ETA()]
    pbar = progressbar.ProgressBar(maxval=len(image_paths), widgets=widgets).start()

    for i in np.arange(0, len(image_paths), BATCH_SIZE):
        batch_paths = image_paths[i : i+BATCH_SIZE]
        batch_labels = labels[i : i+BATCH_SIZE]
        batch_images = []

        for (j, image_path) in enumerate(image_paths):
            image = load_img(image_path, target_size=TARGET_SIZE)
            image = img_to_array(image)
            image = np.expand_dims(image, axis=0)
            image = imagenet_utils.preprocess_input(image)
            batch_images.append(image)

        batch_images = np.vstack(batch_images)
        features = model.predict(batch_images, batch_size=BATCH_SIZE)
        features = features.reshape((features.shape[0]), 512 * 7 * 7)
        dataset.add(features, batch_labels)
        pbar.update(i)

    dataset.close()
    pbar.finish()

if __name__ == '__main__':
    main()