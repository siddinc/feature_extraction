import os


BATCH_SIZE = 32
DATASET_PATH = os.path.abspath('../dataset/animals')
OUTPUT_PATH = os.path.abspath('../output/output.hdf5')
BUFFER_SIZE = 1000
TARGET_SIZE = (224, 224)